all: tor-design-2012.pdf

cell-struct.eps: cell-struct.fig
	fig2dev -L eps $< $@
interaction.eps: interaction.fig
	fig2dev -L eps $< $@
cell-struct.pdf: cell-struct.fig
	fig2dev -L pdf $< $@
interaction.pdf: interaction.fig
	fig2dev -L pdf $< $@

vc._include_.tex: vc tor-design-2012.bib tor-design-2012.tex
	./vc -m && mv vc.tex $@

tor-design-2012.ps: cell-struct.eps interaction.eps tor-design-2012.bib tor-design-2012.tex usenix.sty latex8.bst vc._include_.tex
	latex tor-design-2012.tex
	bibtex tor-design-2012
	latex tor-design-2012.tex
	latex tor-design-2012.tex
	dvips -o $@ tor-design-2012.dvi

tor-design-2012.pdf: cell-struct.pdf interaction.pdf tor-design-2012.bib tor-design-2012.tex usenix.sty latex8.bst vc._include_.tex
	pdflatex tor-design-2012.tex
	bibtex tor-design-2012
	pdflatex tor-design-2012.tex
	pdflatex tor-design-2012.tex

.PHONY: all
